/*
   @file Controller.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Simple example of controller utilisation.
*/

#include <NT_Controller.h>
#include <NT_Task_Manager.h>
#include <EasyTransfer.h>

struct Data_Structure_t
{
  int16_t Buttons;
  int16_t Joy_Ana_Left_X;
  int16_t Joy_Ana_Left_Y;
  int16_t Joy_Ana_Right_X;
  int16_t Joy_Ana_Right_Y;
};

EasyTransfer Transfer;
Data_Structure_t Data;

Controller_t Controller;

Task_t Get_Task;
Task_t Set_Task;

void setup()
{
  // Controller initializations
  Controller.Configure();
  Controller.Attach();
  Controller.Init();

  // Task initializations
  Get_Task.Configure(50, GetSend_Fct);
  Get_Task.Init();

  // EasyTransfer setup
  Serial1.begin(9600);
  Transfer.begin(details(Data), &Serial1);
}

void loop()
{
  Get_Task.Run();
}

void GetSend_Fct()
{
  // Get controls values
  Controller.Get();

  // Map EasyTransfer values to structure values
  Data.Buttons = (int16_t)0;
  Data.Buttons |= (int16_t)Controller.Btn_Up.Value_Op << 0;
  Data.Buttons |= (int16_t)Controller.Btn_Left.Value_Op << 1;
  Data.Buttons |= (int16_t)Controller.Btn_Down.Value_Op << 2;
  Data.Buttons |= (int16_t)Controller.Btn_Right.Value_Op << 3;
  Data.Buttons |= (int16_t)Controller.Btn_1.Value_Op << 4;
  Data.Buttons |= (int16_t)Controller.Btn_2.Value_Op << 5;
  Data.Buttons |= (int16_t)Controller.Btn_3.Value_Op << 6;
  Data.Buttons |= (int16_t)Controller.Btn_4.Value_Op << 7;
  Data.Buttons |= (int16_t)Controller.Btn_Left_Z1.Value_Op << 8;
  Data.Buttons |= (int16_t)Controller.Btn_Left_Z2.Value_Op << 9;
  Data.Buttons |= (int16_t)Controller.Btn_Right_Z1.Value_Op << 10;
  Data.Buttons |= (int16_t)Controller.Btn_Right_Z2.Value_Op << 11;
  Data.Buttons |= (int16_t)Controller.Btn_Select.Value_Op << 12;
  Data.Buttons |= (int16_t)Controller.Btn_Start.Value_Op << 13;
  Data.Buttons |= (int16_t)Controller.Joy_Btn_Left.Value_Op << 14;
  Data.Buttons |= (int16_t)Controller.Joy_Btn_Right.Value_Op << 15;
  Data.Joy_Ana_Left_X = (int16_t)Controller.Joy_Ana_Left_X.Value_Op;
  Data.Joy_Ana_Left_Y = (int16_t)Controller.Joy_Ana_Left_Y.Value_Op;
  Data.Joy_Ana_Right_X = (int16_t)Controller.Joy_Ana_Right_X.Value_Op;
  Data.Joy_Ana_Right_Y = (int16_t)Controller.Joy_Ana_Right_Y.Value_Op;
  
  // EasyTransfer tasks
  Transfer.sendData();
}
